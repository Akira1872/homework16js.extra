
let n = +prompt('Enter a number');

let nFib = (n) => {
    if (n === 0 || n === 1) {
        return n;
    } else if (n > 1) {
        return nFib(n - 1) + nFib(n - 2);
    } else {
        return nFib(n + 1) - nFib(n + 2);
    }
}

console.log(nFib(n));

// В консоль виводить невірне значення. Розумію, що є можливо помилка в формулі, але я не можу її знайти 